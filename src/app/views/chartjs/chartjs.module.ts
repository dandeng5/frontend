//Created by Daniel Deng

import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { debounce } from "typescript-debounce-decorator";

import { ChartJSComponent } from './chartjs.component';
import { ChartJSRoutingModule } from './chartjs-routing.module';
import { FormsModule } from '@angular/forms';
import { polyfill as keyboardEventKeyPolyfill } from 'keyboardevent-key-polyfill';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';
keyboardEventKeyPolyfill();

@NgModule({
  imports: [
    ChartJSRoutingModule,
    ChartsModule,
    FormsModule,
    TextInputAutocompleteModule,
  ],
  declarations: [ ChartJSComponent ]
})
export class ChartJSModule { }
