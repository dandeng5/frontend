//Created by Daniel Deng

import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chart } from 'chart.js';
import { interval } from 'rxjs';
import { debounce } from "typescript-debounce-decorator";
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-get-price-new',
  templateUrl: './chartjs.component.html'
})


export class ChartJSComponent implements OnInit, AfterViewInit {

	@ViewChild('myCanvas') myCanvas: ElementRef;
	public context: CanvasRenderingContext2D;

  chart = []; //holds chart info
  weatherDates = [];
  closing = [];
  ticker: string;
  tempTicker: string;
  modelChanged: Subject<string> = new Subject<string>();
  flag: number = 0;


  constructor(private http: HttpClient) {
    this.modelChanged
        .debounceTime(4000) // wait 300ms after the last event before emitting last event
// only emit if value is different from previous value
        .subscribe(ticker => {
        this.ticker = ticker
      });
  }

  ngOnInit() {
	   interval(15000).subscribe(x => this.update());
  }

  ngAfterViewInit() {

  }

  changed(text: string) {
    this.weatherDates = [];
    this.closing = [];
    this.create();
    this.modelChanged.next(text);
  }

  //this can be done outside an set as an array
  setUpJSON(searchText: string){
    let tickers: string[] = [];
    let variable = require('../dashboard/output_json.json');

    for (let i=0; i<variable.length; i++) {
      tickers.push(variable[i].symbol);
    }
    console.log("wooow" + tickers);
    return tickers.filter(item =>
      item.toLowerCase().includes(searchText.toLowerCase())
    );
  }


  getChoiceLabel(choice: string) {
    return `@${choice} `;
  }




  create(){

  var values = [];
  this.tempTicker = this.ticker.substr(1).slice(0, -1);
  var link = "http://incanada1.conygre.com:9080/prices/" + this.tempTicker + "?periods=50";
  this.http.get(link,{responseType: 'text'}).subscribe(data => {
	var rows = data.split("\n");
      //console.log(rows);
	  for(var i = 1; i < rows.length - 1; i++){
  			var cells = rows[i].split(",");
			var time = cells[0].split(" ");
			var time1 = time[1].split(".");
			this.weatherDates.push(time1[0]);
			this.closing.push(cells[4]);
			values.push({
            "date": time1[0],
            "close": cells[4],
            "open": cells[1],
            "high": cells[2],
            "low": cells[3],
            "volume": cells[5],
            "adjusted": cells[5]
			});
	      }


  	          console.log(this.weatherDates);
	          console.log(this.closing);
	          this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');
  this.chart = new Chart(this.context, {
          type: 'line',
          data: {
          	labels: this.weatherDates,
            datasets: [
              {
                data: this.closing,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {

              yAxes: [{
                display: true
              }]
            }
          }
        });

	});

  }

   update(){
     console.log("called update");
if (this.ticker != '') {
  var values = [];
  this.tempTicker = this.ticker.substr(1).slice(0, -1);
  var link = "http://incanada1.conygre.com:9080/prices/" + this.tempTicker + "?periods=50";
  console.log(link);
  this.http.get(link,{responseType: 'text'}).subscribe(data => {
			var rows = data.split("\n");
	 		var i = rows.length - 2;
  			var cells = rows[i].split(",");
			var time = cells[0].split(" ");
			var time1 = time[1].split(".");
			this.weatherDates.push(time1[0]);
			this.closing.push(cells[4]);
			values.push({
            "date": time1[0],
            "close": cells[4],
            "open": cells[1],
            "high": cells[2],
            "low": cells[3],
            "volume": cells[5],
            "adjusted": cells[5]
			});


	          this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');
  this.chart = new Chart(this.context, {
          type: 'line',
          data: {
          	labels: this.weatherDates,
            datasets: [
              {
                data: this.closing,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {

              yAxes: [{
                display: true
              }]
            }
          }
        });

	});


  }
}
}
