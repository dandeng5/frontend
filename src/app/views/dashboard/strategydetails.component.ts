import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Trade } from './trade'
import { TradeService } from './dashboard.service'
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Strategy } from './strategy';

@Component({
  // tslint:disable-next-line
  selector: 'strategydetails',
  templateUrl: 'strategydetails.html',
})
export class StrategyDetailsComponent implements OnInit {
  constructor() { }

  ngOnInit() {

  }
}
