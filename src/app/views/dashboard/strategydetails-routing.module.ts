//Created by Daniel Deng

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StrategyDetailsComponent } from './strategydetails.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: 'strategy/1',
    component: StrategyDetailsComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class StrategyDetailsModule {}
