//Created by Daniel Deng

import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import { Trade } from './trade';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TradeService {


  constructor(private http:HttpClient) {

  }

  //private userUrl = 'http://localhost:8080/user-portal/user';
	private userUrl = 'http://localhost:8082';




  // public getUsers() {
  //   return this.http.get<User[]>(this.userUrl);
  // }
  //
  // public deleteUser(user) {
  //   return this.http.delete(this.userUrl + "/"+ user.id);
  // }

  public createTrade(trade) : Observable<Trade> {
    console.log("create trade " + trade);
    return this.http.post<Trade>(this.userUrl + "/Create", trade);
  }

  public startTrade(id) {
    console.log("start trade " + id);
    return this.http.put(this.userUrl + "/Start/" + id, null);
  }

  public stopTrade(id) {
    console.log("stop trade " + id);
    console.log(this.userUrl + "/Stop/" + id);
    return this.http.put(this.userUrl + "/Stop/" + id, null);
  }


  public getAllTrades() {
    console.log("getting active trade ");
    return this.http.get(this.userUrl + "/Strategies");
  }


  public refreshPerformance() {
    console.log("grabbing new performance numbers")
    return this.http.get(this.userUrl + "/Performance");
  }





}
