//Created by Daniel Deng

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Trade } from './trade'
import { TradeService } from './dashboard.service'
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Strategy } from './strategy';



declare function require(url: string);

@Component({
  templateUrl: 'dashboard.component.html',
})


export class DashboardComponent implements OnInit {

  trade = new Trade()
  timecreated: number;
  par1: number;
  par2: number;
  par3: number;
  par4: number;
  type: Strategy;
  name: string;
  ticker: string = '';
  show: boolean = false;
  strategyId: number;



  constructor(private tradeService: TradeService) {

  }

  //this can be done outside an set as an array
  setUpJSON(searchText: string): string[] {
    let tickers: string[] = [];
    let variable = require('./output_json.json');

    for (let i=0; i<variable.length; i++) {
      tickers.push(variable[i].symbol);
    }
    console.log("wooow" + tickers);
    return tickers.filter(item =>
      item.toLowerCase().includes(searchText.toLowerCase())
    );
  }


  getChoiceLabel(choice: string) {
    return `@${choice} `;
  }


    // findChoices(searchText: string) {
    //
    //   let tickerArray =
    //
    //   let tickerArray = this.setUpJSON(function(x) => {
    //     console.log(tickerArray);
    //     return x.filter(item => {
    //       item.toLowerCase().includes(searchText.toLowerCase())
    //     });
    //   });
    // }

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnInit(): void {
    this.getAllTrades();
    this.refreshPerformance();
  }



  createTrade(): void {
    this.trade.par1 = this.par1;
    this.trade.par2 = this.par2;
    this.trade.par3 = this.par3;
    this.trade.par4 = this.par4;

    this.trade.type = new Strategy();
    this.trade.type.name = this.name;
    this.trade.ticker = this.ticker;

    console.log(this.trade);
    this.tradeService.createTrade(this.trade)
    .subscribe(result => {
      console.log("wow");
      this.getAllTrades();
    });
  }

  startTrade(): void {
    this.tradeService.startTrade(this.decision)
    .subscribe(result => {
      console.log("starteddddd");
      this.getAllTrades();
    });
  }

  stopTrade(): void {
    this.tradeService.stopTrade(this.decision)
    .subscribe(result => {
      console.log("stopped");
      this.getAllTrades();
    });
  }


  strategies: string[];
  status: string[] = [];
  decision: number;
  transactions: string[];
  performances: string[];

  getAllTrades(): void {
    this.tradeService.getAllTrades()
    .subscribe(result => {
      console.log(result);
      this.strategies = result as string[];
    });
  }

  getAllTransactions(): void {
    console.log(this.strategies);
  }


  refreshPerformance():void {
    this.tradeService.refreshPerformance()
    .subscribe(result => {
      console.log(result);
      this.performances = result as string[];
    });
  }

  getColor(performance) {
    if (performance.charAt(0) == '-') {
      return '#FF4C4C';
    }
    else {
      return '#7FB146';
    }
  }


  adjust(strategy) {
    console.log(strategy.name);
    this.name = strategy.name;
    this.ticker = strategy.ticker;
    this.par1 = strategy.par1;
    this.par2 = strategy.par2;
    this.par3 = strategy.par3;
    this.par4 = strategy.par4;
    console.log(strategy);
  }

  getDetails(transactions, id) {
    console.log(transactions);
    this.show = true;
    this.strategyId = id;
    this.transactions = transactions;
  }

  // populateStatus(result) {
  //   for (let i=0; i<result.length; i++) {
  //     this.status.push(result[i].strategy.active);
  //   }
  // }
  //
  // isPaused = false;
  // text = "Play";
  //
  // click() {
  //   console.log("clicked");
  //   this.isPaused = !this.isPaused;
  //   this.text = this.isPaused ? "Pause" : "Play";
  // }



  clearForm() {
    console.log("clear form");
    this.ticker = null;
    this.par1 = null;
    this.par2 = null;
    this.par3 = null;
    this.par4 = null;
    this.name = null;
  }

}
