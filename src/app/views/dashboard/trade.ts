import { Strategy } from './strategy'

export class Trade {
    ticker: string;
    timecreated: number;
    par1: number;
    par2: number;
    par3: number;
    par4: number;
    type: Strategy;
}
