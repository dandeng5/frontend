export const navItems = [
  {
    name: 'Trading Dashboard',
    url: '/dashboard',
    icon: 'icon-globe',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Performance'
  },
  {
    name: 'Profit/Loss',
    url: '/base',
    icon: 'icon-calculator',
  },
  {
    name: 'Chart',
    url: '/charts',
    icon: 'icon-chart',
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Extras',
  },
  {
    name: 'Pages',
    url: '/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/register',
        icon: 'icon-star'
      }
    ]
  }
];
